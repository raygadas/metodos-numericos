%
% Andres Raygadas
% 18-Agosto-2017
%
% Este programa implementa el metodo de biseccion
% para encontrar raices de ecuacion lineales.
%
% Datos de entrada:
%
% -- Funcion continua
% -- Intervalo (para encontrar la raiz)
%
% Datos de salida:
% -- Raiz
% -- ERPa
%

clc
clear all
format long

% Cadena es la que le pido al usuario y la guardo como una cadena de caracteres
cadena = input('Dame la funcion continua (entre apostofes): ');

% Pasamos la cadena a una funcion con inline
fun = inline(cadena);

% Pedir intervalo:
a = input('Dame el extremo inferior del intervalo: ');
b = input('Dame el extremo superior del intervalo: ');

% Pedir tolerancia
tol = input('Dame la tolerancia: ');
anterior = 0;
ERPa = 100;


% Evaluamos en los extremos del intervalo
fa = fun(a);
fb = fun(b);

% Revisamos si los datos de entrada evaluan uno en - y otro en +
while ( fa * fb > 0 )
  % Pedir nuevos datos
  fprintf('Los datos no son validos, ingreso nuevos datos\n')
  a = input('Dame el extremo inferior del intervalo: ');
  b = input('Dame el extremo superior del intervalo: ');
  
  % Evaluamos los nuevos entremos del intervalo
  fa = fun(a);
  fb = fun(b);
end


% Si los intervalos evaluan uno en + y otro en -, seguimos 
 if ( fa*fb < 0) %if1
  while ERPa > tol %while
    c = (a+b) / 2;
    % el actual es c
    ERPa = abs( (c - anterior) / c ) * 100;
    fc = fun(c);
   
    if fc == 0 %if2
    % mostrar el resultado y terminar
      disp('Que suertudo, encontraste la raiz');
      fprintf(' La raiz es : %d\n', c)
      return
      
    else
    % seguir trabajndo
      if fa * fc < 0 %if3
        % la raiz esta a la izquierda
        b = c;
        fb = fc;
      else
        % la raiz esta a la derecha
        a = c;
        fa = fc;
      end %if3
      anterior = c;
      
    end %if2
   end %while
   disp('La raiz es: ')
   disp(c)
   disp('El ERP es: ')
   disp(ERPa);
end %if1

