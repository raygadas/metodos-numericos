%
% Andres Raygadas
% 28-Agosto-2017
%
% Este programa implementa el metodo de Newton
% para encontrar raices de ecuacion no lineales.
%
% Datos de entrada:
%
% -- Funcion continua
% -- Funcion derivable
% -- Derivada de la funcion
% -- Intervalo para graficar (limite inferior y superior)
% -- Error toleardo
%
% Datos de salida:
% -- Número de iteración
% -- Valor de x en la iteración
% -- Valor de f(x) en la iteración
% -- Valor del ERPa
%

clear
clc
format long

% Pedir funcion continua y derivable para encontrar la raiz. (Y su derivada)
fun = @(x) (exp(-x)-x);
deriv = @(x) (-exp(-x) - 1);

% Funcion para encontrar el ERPa
get_erpa = @(a, b) (abs((a-b)/a) * 100);

% Pedir intervalo y establecer funciin a la variable 'y'
x = linspace(.5, .7);
y = fun(x);

% Graficar la funcion dentro del intervalo
plot(x, y)
grid on

% Establecer el erpa inicial en 100 y pedir tolerancia
erpa = 100;
error_tol = eps;

% Pedir primera aproximación a la raiz
x_actual = .5;

% Evaluar en f(x) la primera aproximación
fx_actual = fun(x_actual)

% Establecer contador de pasos en 0
cont = 0;

% Iteramos mientras el error en x sea mayor a la tolerancia
while (erpa > error_tol)
  % Calcular nuevo x_actual y actualizar x_anterior
  x_anterior = x_actual;
  x_actual = x_anterior - (fun(x_anterior) / deriv(x_anterior));
  
  % Calcular f(x) en el punto acutal
  fx_actual = fun(x_actual);
  
  % Calcular nueva erpa
  erpa = get_erpa(x_actual, x_anterior);
  
  % Aumentar de paso
  cont++;
  
  % Imprimir la iteracion, valor de x, de f(x) y el ERPa
  fprintf('Paso: %d \n', cont)
  fprintf('Valor x: %.9f \n', x_actual)
  fprintf('f(x): %.9f \n', fx_actual)
  fprintf('ERPa: %.9f \n\n', erpa)
end
 
 