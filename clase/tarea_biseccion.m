%
% Andres Raygadas
% 28-Agosto-2017
%
% Este programa implementa el metodo de biseccion
% para encontrar raices de ecuacion no lineales.
%
% Datos de entrada:
% - 
%
% Datos de salida:
% -- Raiz
% -- Numero de paso y:
%     -- Valor de limite inferior
%     -- Valor de limite superior
%     -- Erpa



clear
clc
format long


% Declarar la funcion a evaluar
fun = @(x) (abs(x.^2 .* cos(sqrt(x))) - 5);

% Declarar ejes (x, y) y graficar
x = linspace(0, 25);
y = fun(x);
plot(x, y);
grid on

% Establecer erpa inicial y tolerancia
tol = .001;
erpa = 1000;

% Asignar limite inferior y superior iniciales
% Evaluar la funcion en esos puntos
a = 0;
b = 5;
anterior = 0;
fa = fun(a);
fb = fun(b);

% Establecer contador de pasos en 0
cont = 0;

% Verificar que el valor de 'y' en ambos puntos sea de signo contrario
if(fa * fb >= 0)
  disp("Las dos aproximaciones no son validas.")
  return;

% Si fa y fb son de signo contrario, seguir
else 
  % Mientras el erpa sea mayor a la tolerancia, iterar
  while (erpa > tol)
    % Encontrar y evaluar en punto medio
    c = (a + b)/2;
    fc = fun(c);
    
    % Calcular nuevo erpa
    erpa = (abs((c-anterior)/c) * 100);
    
    % Si fc es exactamente 0, c es la raiz
    if (fc == 0)
      disp('¡Encontraste la raiz!')
      return
    % Si fc no es exactamente 0, continuar
    else
      % Si fc es mayor a cero, mover el limite superior 'b' a 'c'
      if (fc > 0)
        b = c;
        fb = fc;  
      % Si fc es menor a cero, mover el limite inferior 'a' a 'c'
      else
        a = c;
        fa = fc;
      end
    end
    
    % Actualizar el valor anterior de c, necesario para el erpa
    anterior = c;
    
    % Imprimir resultados de la iteracion
    fprintf('Paso: %d \n', cont);
    fprintf('a: %f \n', a);
    fprintf('b: %f \n', b);
    fprintf('erpa: %f \n', erpa);
    fprintf('tol: %f \n\n', tol);
    
   
    cont++;
  end
  
  % Imprimir resultado final de la raiz
  disp('La raiz es: ')
  disp(c)
end


