clear
clc

fun = @(x) x.^2 + 2*x - 15;
x = linspace(-6, 6);
y = fun(x);
plot(x,y)
grid on