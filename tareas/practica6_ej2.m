% Autor: Andres Raygadas
% 10 / Noviembre / 2017
% EJERCICIO 2 LA PRÁCTICA 6
%
close all
clear all
clc

%----------------------------------------------------------------------%

% INCISO A y 

% Limites de x
a = 0;
b = 20;

% Tamaño de paso 2 con n subintervalos
h = 2;
n = (b - a) / (h);

% Generamos el vector 'y' y 'x' de a hasta b con ancho 'h'
x = a:h:b;
y = [0, 1.8, 2, 4, 4, 6, 4, 3.6, 3.4, 2.8, 0];

% Inicializar matriz de integrales 
I = zeros(n, 1);


for i=1:n  
  base = x(i+1) - x(i);
  altureProm = ((y(i+1) + y(i)) / 2);
  
  I(i) = base * altureProm;

  figure(1)
  plot([x(i + 1) x(i + 1)],[0 y(i + 1)],'g')
  hold on
  plot([x(i) x(i+1)],[y(i) y(i+1)], 'g')
  hold on
  
end

integral = sum(I);
display('Integral con tamaño de intervalo 2: ')
display(integral)

%----------------------------------------------------------------------%

% INCISO A y B
% Limites de x
a = 0;
b = 20;

% Tamaño de paso 4 con n subintervalos
h = 4;
n = (b - a) / (h);

% Generamos el vector 'y' y 'x' de a hasta b con ancho 'h'
x = a:h:b;
y = [0, 2, 4, 4, 3.4, 0];

% Inicializar matriz de integrales 
I = zeros(n, 1);


for i=1:n  
  base = x(i+1) - x(i);
  altureProm = ((y(i+1) + y(i)) / 2);
  
  I(i) = base * altureProm;

  
  figure(2)
  plot([x(i + 1) x(i + 1)],[0 y(i + 1)],'g')
  hold on
  plot([x(i) x(i+1)],[y(i) y(i+1)], 'g')
  hold on
 
end

integral = sum(I);
display('Integral con tamaño de intervalo 4: ')
display(integral)

%----------------------------------------------------------------------%

% INCISO C y D - SIMPSON 1/3

% INCISO A y B
% Limites de x
a = 0;
b = 20;

% Tamaño de paso 4 con n subintervalos
n = 5;
h = (b - a) / (2*n);

% Generamos el vector 'y' y 'x' de a hasta b con ancho 'h'
x = a:h:b;
y = [0, 1.8, 2, 4, 4, 6, 4, 3.6, 3.4, 2.8, 0];

% Inicializar matriz de integrales 
I = zeros(n, 1);

% Grafica de la pared izquierda de la primer aplicacion.
figure(3)
plot([x(1) x(1)],[0 y(1)],'g')
hold on

for i = 1:n
  base = x(2*i + 1) - x(2*i - 1);
  alturaProm = (y(2*i - 1) + 4*y(2*i) + y(2*i + 1))/6;
  I(i) = base*alturaProm;
  
  % Grafica de la pared derecha de la aplicacion
  plot([x(2*i + 1) x(2*i + 1)],[0 y(2*i + 1)],'g')
  hold on
  
  % Grafica de la pared central de cada aplicacion
  plot([x(2*i) x(2*i)],[0 y(2*i)],'y')
  hold on
  
  % Grafica del techo de cada aplicacion (parabola)
  x1 = linspace(x(2*i-1),x(2*i+1));
  y1 = zeros(100,1);
  A = [x(2*i-1) y(2*i-1);x(2*i) y(2*i);x(2*i+1) y(2*i+1)];
  
  for k = 1:100
    L = ones(3,1);
    
    for z = 1:3
      for j = 1:3
        if (z != j)
          L(z) = ((x1(k) - A(j,1))/(A(z,1) - A(j,1))).*L(z);
        end
      end
    end
    y1(k) = sum(L.*A(:,2));
  end
  plot(x1,y1,'r')
  hold on
end

integral = sum(I);
display('Integral con simpson 1/3: ')
display(integral)

%----------------------------------------------------------------------%

% INCISO F y G - SIMPSON 1/3, 2*SIMPSON 3/8, SIMPSON 1/3


% SIMPSON 1/3

% Limites de x
a = 0;
b = 4;

% 1 APLICACION DE LA REGLA CON H NUMERO DE PASOS
n = 1;
h = (b - a) / (2*n);

% Generamos el vector 'y' y 'x' de a hasta b con ancho 'h'
x = a:h:b;
y = [0, 1.8, 2];

% Inicializar matriz de integrales 
I = zeros(n, 1);

% Grafica de la pared izquierda de la primer aplicacion.
figure(4)
plot([x(1) x(1)],[0 y(1)],'g')
hold on

for i = 1:n
  base = x(2*i + 1) - x(2*i - 1);
  alturaProm = (y(2*i - 1) + 4*y(2*i) + y(2*i + 1))/6;
  I(i) = base*alturaProm;
  
  % Grafica de la pared derecha de la aplicacion
  plot([x(2*i + 1) x(2*i + 1)],[0 y(2*i + 1)],'g')
  hold on
  
  % Grafica de la pared central de cada aplicacion
  plot([x(2*i) x(2*i)],[0 y(2*i)],'y')
  hold on
  
  % Grafica del techo de cada aplicacion (parabola)
  x1 = linspace(x(2*i-1),x(2*i+1));
  y1 = zeros(100,1);
  A = [x(2*i-1) y(2*i-1);x(2*i) y(2*i);x(2*i+1) y(2*i+1)];
  
  for k = 1:100
    L = ones(3,1);
    
    for z = 1:3
      for j = 1:3
        if (z != j)
          L(z) = ((x1(k) - A(j,1))/(A(z,1) - A(j,1))).*L(z);
        end
      end
    end
    y1(k) = sum(L.*A(:,2));
  end
  plot(x1,y1,'r')
  hold on
end

integral1 = sum(I);
%FIN SIMPSON !/3

%SIMPSON 3/8
a = 4;
b = 16;

% Tamaño de paso 4 con n subintervalos
n = 2;
h = (b-a)/(3*n);

% Generamos el vector 'y' y 'x' de a hasta b con ancho 'h'
x = a:h:b;
y = [2, 4, 4, 6, 4, 3.6, 3.4];


% Inicializar matriz de integrales 
I = zeros(n, 1);

for i = 1:n
  base = x(3*i + 1) - x(3*i - 2);
  alturaProm = (y(3*i - 2) + 3*y(3*i - 1) + 3*y(3*i) + y(3*i + 1))/8;
  I(i) = base*alturaProm;
  
  % Grafica de la pared derecha de la aplicacion
  plot([x(3*i + 1) x(3*i + 1)],[0 y(3*i + 1)],'g')
  hold on
  
  % Grafica de las paredes centrales de cada aplicacion
  plot([x(3*i-1) x(3*i-1)],[0 y(3*i-1)],'y')
  hold on
  plot([x(3*i) x(3*i)],[0 y(3*i)],'y')
  hold on
  
  % Grafica del techo de cada aplicacion (cúbica)
  x1 = linspace(x(3*i-2),x(3*i+1));
  y1 = zeros(100,1);
  A = [x(3*i - 2),y(3*i - 2);x(3*i - 1) y(3*i - 1);x(3*i) y(3*i);x(3*i + 1) y(3*i + 1)];
  
  for k = 1:100
    L = ones(4,1);
    
    for z = 1:4
      for j = 1:4
        if (z != j)
          L(z) = ((x1(k) - A(j,1))/(A(z,1) - A(j,1))).*L(z);
        end
      end
    end
    y1(k) = sum(L.*A(:,2));
  end
  plot(x1,y1,'r')
  hold on
end

integral2 = sum(I);
%FIN SIMPSON 3/8

% SIMPSON 1/3
a = 16;
b = 20;

% 1 APLICACION DE LA REGLA CON H NUMERO DE PASOS
n = 1;
h = (b - a) / (2*n);

% Generamos el vector 'y' y 'x' de a hasta b con ancho 'h'
x = a:h:b;
y = [3.4, 2.8, 0];

% Inicializar matriz de integrales 
I = zeros(n, 1);

% Grafica de la pared izquierda de la primer aplicacion.
plot([x(1) x(1)],[0 y(1)],'g')
hold on

for i = 1:n
  base = x(2*i + 1) - x(2*i - 1);
  alturaProm = (y(2*i - 1) + 4*y(2*i) + y(2*i + 1))/6;
  I(i) = base*alturaProm;
  
  % Grafica de la pared derecha de la aplicacion
  plot([x(2*i + 1) x(2*i + 1)],[0 y(2*i + 1)],'g')
  hold on
  
  % Grafica de la pared central de cada aplicacion
  plot([x(2*i) x(2*i)],[0 y(2*i)],'y')
  hold on
  
  % Grafica del techo de cada aplicacion (parabola)
  x1 = linspace(x(2*i-1),x(2*i+1));
  y1 = zeros(100,1);
  A = [x(2*i-1) y(2*i-1);x(2*i) y(2*i);x(2*i+1) y(2*i+1)];
  
  for k = 1:100
    L = ones(3,1);
    
    for z = 1:3
      for j = 1:3
        if (z != j)
          L(z) = ((x1(k) - A(j,1))/(A(z,1) - A(j,1))).*L(z);
        end
      end
    end
    y1(k) = sum(L.*A(:,2));
  end
  plot(x1,y1,'r')
  hold on
end

integral3 = sum(I);
%FIN SIMPSON !/3

integral_total = integral1 + integral2 + integral3;
disp('Integral con Simspon 1/3, 2 Simpson 3/8 y Simpson 1/3')
disp(integral_total)
