close all
clear
clc

fun = @(x) exp(x) ./ x;

a = 0.2;
b = 4;

x1 = linspace(a,b);
y1 = fun(x1);

plot(x1, y1, 'b');
hold on;

% n = input('Cuantas aplicaciones de simpson 3/8 deseas?');
n = 16;

h = (b - a) / (3 * n);

x = a:h:b;
y = fun(x);

I = zeros(n, 1);

plot([x(1) x(1)], [0 y(1)], 'g');
hold on;

for i=1:n

    base = x(3 * i + 1) - x(3 * i - 2);

    altureProm = (y(3 * i - 2) + 3 * y(3 * i  - 1) + 3 *y(3*i) + y(3*i + 1) ) / 8;
    I(i) = base * altureProm;

    plot([x(3*i + 1) x(3*i + 1)], [0 y(3*i + 1)], 'g')
    hold on;

    plot([x(3*i - 1) x(3*i - 1)], [0 y(3*i - 1)], 'y')
    hold on;

    plot([x(3*i) x(3*i)], [0 y(3*i)], 'y')
    hold on;

    x1 = linspace(x(3*i - 1), x(3*i + 1));
    y1 = zeros(100, 1);
    A = [x(3*i - 2) y(3*i -2); x(3*i - 1) y(3*i - 1); x(3*i) y(3*i); x(3*i + 1) y(3*i + 1)];
    %LAGRANGE

    for k = 1:100
        L = ones(4, 1);
        for z = 1:4
            for j = 1:4
                if j != z
                    L(z) = ((x1(k) - A(j, 1))/ (A(z, 1) - A(j, 1))) .* L(z);
                end
            end
        end
        y1(k) = sum(L.*A(:, 2));
    end
    plot(x1, y1, 'm');
    hold on;

end
    
    

integral = sum(I)
Real = quadl(fun,a,b)

ERPa = abs((Real - integral)/ Real) * 100

