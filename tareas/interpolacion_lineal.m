% Interpolación Lineal
% ANDRES RAYGADAS
% 24/Oct/2017

clc
clear all
close all

%--------------------------INICIO TABLA1--------------------------%

% Crear y ordenar la matriz
A = [12 2.48490665; 
      8 2.079441542; 
      14 2.63905733; 
      6 1.791759469];
A = sortrows(A);

% Graficar
figure(1)
plot(A(:, 1), A(:, 2), 'b');
hold on
plot(A(:, 1), A(:, 2), 'o');
hold on

% Encontrar valores para la función
x = 10;
if (!(x >= min(A(:, 1)) && x <= max(A(:, 1))))
    disp('ERROR')
    return;
end
pos = sum(A(:, 1) < x);
x0 = A(pos, 1);
y0 = A(pos, 2);
x1 = A(pos + 1, 1);
y1 = A(pos + 1, 2);

% Formula de interpolacion lineal
y = y0 + ( (y1 - y0) / (x1 - x0)) * (x - x0);
plot(x, y, '*');

% ERPv
verdadero = log(10);
erpv = (abs ((verdadero-y)/verdadero) ) * 100;

% Resultados:
display("TABLA 1: ")
fprintf(' %f   =    %f + ( (%f - %f) / (%f - %f) ) * (%f - %f)\n',y,y0,y1,y0,x1,x0,x,x0)
fprintf(' Estimacion para x = 10:\t  %f \n', y)
fprintf(' ERPv: %f\n\n\n', erpv)
%--------------------------FIN TABLA1-----------------------------%





%--------------------------INICIO TABLA2--------------------------%
% Crear y ordenar la matriz
A = [ 11 2.397895273; 
      8 2.079441542; 
      12 2.48490665; 
      9 2.197224577];
A = sortrows(A);

% Graficar
figure(2)
plot(A(:, 1), A(:, 2), 'g');
hold on
plot(A(:, 1), A(:, 2), 'o');
hold on

% Encontrar valores para la función
x = 10;
if (!(x >= min(A(:, 1)) && x <= max(A(:, 1))))
    disp('ERROR');
    return;
end
pos = sum(A(:, 1) < x);
x0 = A(pos, 1);
y0 = A(pos, 2);
x1 = A(pos + 1, 1);
y1 = A(pos + 1, 2);

% Formula de interpolacion lineal
y = y0 + ( (y1 - y0) / (x1 - x0)) * (x - x0);
plot(x, y, '*');

% ERPv
verdadero = log(10);
erpv = (abs ((verdadero-y)/verdadero) ) * 100;


% Resultados:
display("TABLA 2: ");
fprintf(' %f   =    %f + ( (%f - %f) / (%f - %f) ) * (%f - %f)\n',y,y0,y1,y0,x1,x0,x,x0)
fprintf(' Estimacion para x = 10:\t  %f \n', y)
fprintf(' ERPv: %f\n\n\n', erpv)


%--------------------------FIN TABLA2-----------------------------%