clc
close all
clear all
A = [5 17; 10 24; 15 31; 20 33; 25 37; 30 37; 35 40; 40 40; 45 42; 50 41]
plot(A(:, 1), A(:, 2), 'o')
hold on

%--------------------------INICIO DE LA LÍNEA RECTA--------------------------%
%
% 
% 
% 
disp('')
disp('LINEA RECTA: ')

% Numero de filas
n = size(A)(1);

% valores para la matriz de coeficientes
xi = sum(A(:, 1));
xi_sqr = sum(A(:, 1).^ 2);

% Crear la matriz de coeficientes
Coef = [ n  xi;
         xi xi_sqr ];

% Calcular los valores y crear el vector del lado derecho 
yi = sum(A(:, 2));
xiyi = sum(A(:, 1) .* A(:, 2));
derecho = [yi; xiyi];

% Encontrar vector vertical de soluciones al sistema
sol = Coef\derecho;

% Encontrar y del modelo y guardarlo en la tercera columna
A(:, 3) = sol(1) + sol(2) * (A(:, 1));

% Definir x graficar
x = linspace(min(A(:,1))-5, max(A(:,1))+5);

% Encontrar a0, a1 para hacer la gráfica de la recta, que son la pendiente y el cruce con el eje 'y'
% dados en la solucion al sistema de ecuaciones
a1 = sol(2)
a0 = sol(1)
y = a1*x + a0;

% Grafica de la recta
plot(x, y, 'g')
hold on

% Encontrar Sr = sum((y_medida - y_modelo)^2)
Sr = sum((A(:,2) - A(:,3)).^2);

% Encontrar St usando el promedio de y_medida
avg_y = mean(A(:,2));
St = sum((A(:,2)-avg_y).^2);

% Calcular la desviacion estandar (dos maneras distintas)
Sy = sqrt(St/(n-1));
Sy = std(A(:, 2));

% Encontrar el error estandar
Syx = sqrt(Sr/(n-2));

% Encontrar el coeficiente de correlacion
r2 = (St - Sr) / St;
r = sqrt(r2);

% Encontrar el PIM
PIEM = r2 * 100;

% Imprimir resultados
disp('Modelo de un polinomio de grado 2 (parabola):')
fprintf(' Y   =    %f + %f X \n', a0,a1)
disp('El error estandar del estimado:')
disp(Syx)
disp('Desviacion estandar: ')
disp(Sy)
disp('Coeficiente de correlacion:')
disp(r)
disp('PIEM: ')
disp(PIEM)
if Syx < Sy
  disp('El modelo es adecuado. El error estandar es menor a la desviacion estandar')
else
  disp('El modelo no es adecuado. El error estandar NO es menor a la desviacion estandar')
  
end

disp('')
%--------------------------FIN DE LINEA RECTA--------------------------%


%--------------------------INICIO DE POTENCIA--------------------------%
%
% 
% 
% 

%--------------------------FIN DE POTENCIA--------------------------%



%--------------------------INICIO DE LA PARABOLA--------------------------%
%
% Este programa calcula un modelo de regresion por minimos 
% cuadrados Polinomial de grado 2 (parabola)
% 

disp('')
disp('PARABOLA: ')

% Numero de columnas y de filas
[m, n] = size(A);

% Calcular los parametros del polinomio:
Coef = [m sum(A(:,1)) sum(A(:,1).^2); sum(A(:,1)) sum(A(:,1).^2) sum(A(:,1).^3); sum(A(:,1).^2) sum(A(:,1).^3) sum(A(:,1).^4);];
der = [sum(A(:,2)); sum(A(:,1).*A(:,2)); sum(A(:,1).^2.*A(:,2))];
sol = Coef\der;

a0 = sol(1);
a1 = sol(2);
a2 = sol(3);

% Grafica del polinomio:
x3 = linspace(min(A(:,1))-5, max(A(:,1))+5);
y3 = a0 + a1*x3 + a2*x3.^2;
plot(x3, y3, 'r')
hold on

A(:, 3) = (a0 + a1*A(:,1) + a2*A(:,1).^2);

% Sr
Sr = sum((A(:, 2) - A(:,3)).^2);

% Calcular el error estandar del estimado:
Syx = sqrt(Sr/(m-3));

% Calcular la desviacion estandar:
Sy = std(A(:, 2));

% Calcular St
St = sum((A(:, 2) - mean(A(:,2))).^2);

% Calcular r, coeficiente de correlacion
r = sqrt((St - Sr)/St);


% Calcular PIEM, Porcentaje de incertidumbre explicada por el modelo
PIEM = r^2 * 100;


% Imprimir resultados
disp('Modelo de un polinomio de grado 2 (parabola):')
fprintf(' Y   =    %f + %f X  +  %f X^2\n', a0,a1,a2)
disp('El error estandar del estimado:')
disp(Syx)
disp('Desviacion estandar: ')
disp(Sy)
disp('Coeficiente de correlacion:')
disp(r)
disp('PIEM: ')
disp(PIEM)
if Syx < Sy
  disp('El modelo es adecuado. El error estandar es menor a la desviacion estandar')
else
  disp('El modelo no es adecuado. El error estandar NO es menor a la desviacion estandar')
end

disp('')
%--------------------------FIN DE LA PARABOLA--------------------------%

