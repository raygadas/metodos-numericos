% Autor: Andres Raygadas
% 10 / Noviembre / 2017
% PRÁCTICA 7
% Este programa encuentra la solucion numérica a una EDO de primer orden utilizando y comparando diferentes métodos de Runje-Kutta.
% Se requiere de la EDO, el intervalo y de una condición inicial. Se muestran las gráficas y los resultados en una tabla.

close all
clear all
clc

% EDO
der = @(x,y) y.*sin(x).*sin(x).*sin(x);

% Intervalo
a = 0;
b = 3;

% Condici[on inicial.
cond = [0 1];

% Tamaño de paso:
h = 0.5;

% Solución Analítica.
fun = @(x) exp(-cos(x) + cos(x).*cos(x).*cos(x)./3 + 2./3);
% Gráfica de la solución analítica.
x = a:h:b;
x = x';
yReal = fun(x);
plot(x, yReal, 'r')
hold on

% Inicia EULER -----------------------------------
yEuler = zeros(numel(x),1);
yEuler(1) = cond(2);

for i = 1:(numel(x)-1)
  pendiente = der(x(i),yEuler(i));
  yEuler(i+1) = yEuler(i) + pendiente*h;
end
plot(x, yEuler, 'g')
hold on
% Termina EULER ----------------------------------

% Inicia HEUN ------------------------------------
yHeun = zeros(numel(x),1);
yHeun(1) = cond(2);

for i = 1:(numel(x)-1)
  k1 = der(x(i),yHeun(i));
  k2 = der(x(i)+h,yHeun(i)+k1*h);
  pendiente = (k1+k2)/2;
  yHeun(i+1) = yHeun(i) + pendiente*h;
end
plot(x, yHeun, 'b')
hold on
% Termina HEUN ------------------------------------

% Inicia Punto Medio ------------------------------
yPM = zeros(numel(x),1);
yPM(1) = cond(2);

for i = 1:(numel(x)-1)
  k1 = der(x(i),yPM(i));
  k2 = der(x(i)+h/2,yPM(i)+k1*h/2);
  pendiente = k2;
  yPM(i+1) = yPM(i) + pendiente*h;
end
plot(x, yPM, 'y')
hold on
% Termina Punto Medio ------------------------------

% Inicia Ralston -----------------------------------
yRalston = zeros(numel(x),1);
yRalston(1) = cond(2);

for i = 1:(numel(x)-1)
  k1 = der(x(i),yRalston(i));
  k2 = der(x(i)+3/4*h,yRalston(i)+3/4*k1*h);
  pendiente = (1/3*k1 + 2/3*k2);
  yRalston(i+1) = yRalston(i) + pendiente*h;
end
plot(x, yRalston, 'c')
hold on
% Termina Ralston -----------------------------------

% Inicia Butcher ------------------------------------
yButcher = zeros(numel(x),1);
yButcher(1) = cond(2);

for i = 1:(numel(x)-1)
  k1 = der(x(i),yButcher(i));
  k2 = der(x(i)+1/4*h,yButcher(i)+1/4*k1*h);
  k3 = der(x(i)+1/4*h,yButcher(i)+1/8*k1*h+1/8*k2*h);
  k4 = der(x(i)+1/2*h,yButcher(i)-1/2*k2*h+k3*h);
  k5 = der(x(i)+3/4*h,yButcher(i)+3/16*k1*h+9/16*k4*h);
  k6 = der(x(i)+h,yButcher(i)-3/7*k1*h+2/7*k2*h+12/7*k3*h-12/7*k4*h+8/7*k5*h);
  pendiente = 1/90*(7*k1 + 32*k3 + 12*k4 + 32*k5 + 7*k6);
  yButcher(i+1) = yButcher(i) + pendiente*h;
end
plot(x, yButcher, 'm')
hold on
% Termina Butcher -----------------------------------

legend("Real","Euler","Heun", "Punto Medio", "Ralston", "Butcher", "location",'NorthWest');

% Desplegar tabla de resultados:
disp("   x         yReal     yEuler    yHeun     yPM 	    yRalston   yButcher")
disp([x,yReal,yEuler,yHeun,yPM,yRalston,yButcher])

disp('En este caso el mejor método es el de Butcher. Cuando damos un tamaño de paso .5')
disp('Butcher está practicamente empalmado con la Solcución Analítica. A medida que disminuimos')
disp('el tamaño de paso, las demás funciones también se aproximan, menos Euler porque es de grado')
disp('1 y en este caso en particular no es apropiado. Como conclusión, Euler sería el método más')
disp('apropiado si la Solución Analítica es una recta, ya que es de grado 1. Por otro lado, Heun, Punto Medio ')
disp('y Ralston, son de segundo grado y, finalmente, Butcher es de grado 5 y cada método es más ')
disp('conveniente, logicamente, para aproximar Soluciones Analíticas del mismo grado o cercano.')
	
