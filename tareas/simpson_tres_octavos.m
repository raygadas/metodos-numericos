% Este programa calcula una estimacion del area bajo la curva implementando la regla de simpson 3/8.
% Codigo desarrollado por Diego Betanzos Esquer - A00231974
% 07 de noviembre de 2017.

close all
clear all
clc

% INCISO A y B
% Limites de x
a = 4;
b = 16;

% Tamaño de paso 4 con n subintervalos
n = 2;
h = (b-a)/(3*n);

% Generamos el vector 'y' y 'x' de a hasta b con ancho 'h'
x = a:h:b;
y = [2, 4, 4, 6, 4, 3.6, 3.4];


% Inicializar matriz de integrales 
I = zeros(n, 1);

for i = 1:n
  base = x(3*i + 1) - x(3*i - 2);
  alturaProm = (y(3*i - 2) + 3*y(3*i - 1) + 3*y(3*i) + y(3*i + 1))/8;
  I(i) = base*alturaProm;
  
  % Grafica de la pared derecha de la aplicacion
  plot([x(3*i + 1) x(3*i + 1)],[0 y(3*i + 1)],'g')
  hold on
  
  % Grafica de las paredes centrales de cada aplicacion
  plot([x(3*i-1) x(3*i-1)],[0 y(3*i-1)],'y')
  hold on
  plot([x(3*i) x(3*i)],[0 y(3*i)],'y')
  hold on
  
  % Grafica del techo de cada aplicacion (cúbica)
  x1 = linspace(x(3*i-2),x(3*i+1));
  y1 = zeros(100,1);
  A = [x(3*i - 2),y(3*i - 2);x(3*i - 1) y(3*i - 1);x(3*i) y(3*i);x(3*i + 1) y(3*i + 1)];
  
  for k = 1:100
    L = ones(4,1);
    
    for z = 1:4
      for j = 1:4
        if (z != j)
          L(z) = ((x1(k) - A(j,1))/(A(z,1) - A(j,1))).*L(z);
        end
      end
    end
    y1(k) = sum(L.*A(:,2));
  end
  plot(x1,y1,'r')
  hold on
end

Integral = sum(I)
