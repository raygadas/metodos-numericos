clear;
clc;

fun = @(x) exp(x) ./ x;

a = 0.2;
b = 4;


x1 = linspace(a,b);
y1 = fun(x1);

plot(x1, y1, 'b');
hold on;

n = 64;

    h = (b - a) / (2 * n);

    x = a:h:b;
    y = fun(x);

    I = zeros(n, 1);

    plot([x(1) x(1)], [0 y(1)], 'g');
    hold on;


    for i=1:n
        base = x(2 * i + 1) - x( 2 * i - 1);

        altureProm = ((y(2 * i + 1) + y(2 * i - 1)) / 2);

        I(i) = base * altureProm;

        plot([x(2 * i + 1) x(2 * i + 1)], [0 y( 2 * i + 1)], 'g')
        hold on;

        plot([x(2*i) x(2*i)], [0 y(2*i)], 'y')
        hold on;
    end


    integral = sum(I);
    Real = quadl(fun,a,b);
    ERPa = abs((Real - integral)/ Real) * 100;


    %disp([n, integral, ERPa])
    printf('%i,%f,%f\n',n,integral,ERPa);

