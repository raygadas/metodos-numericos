clear
clc

% Ciclo for
fprintf('\n FOR \n')
for x = [1 3 5 7]
				disp(x)
end


% Ciclo while
fprintf('\n WHILE \n')
n = 0;
while( n < 5)
  fprintf('Numero: %d\n', n);
  n++;
end
            

% Ciclo if
fprintf('\n IF \n')
x = input('Ingresa un valor: ');
if x<10 
  fprintf('El numero %d es menor a 10\n',x);
elseif x > 10
  fprintf('El numero %d es mayor a 10\n',x);
else
  fprintf('El numero es 10\n');
end


% Gráfica 2D
fprintf('\n GRAFICA 2D \n')
x = linspace (1, 100)
y = x.^2
figure
plot(x, y)

% Epsilon
fprintf('\n EPS \n')
format long
my_eps = 1;
count = 0;
while 1.0 + (my_eps/2) > 1.0
  my_eps = my_eps/2;
  count++;
end
fprintf('Eps calculado: ') 
disp(my_eps)
fprintf('Eps de matlab: ')
disp(eps)
fprintf('Contador: %d \n', count)


