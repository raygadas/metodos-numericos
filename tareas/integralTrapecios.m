% Este programa calcula una estimacion del area bajo la curva implementando la regla de simpson 1/3.
% Codigo desarrollado por Diego Betanzos Esquer - A00231974
% 03 de noviembre de 2017.

close all
clear all
clc

% fun = @(x) 0.2 + 25*x - 200*x.^2 + 675*x.^3 -900*x.^4 + 400*x.^5;
fun = @(x) 1-e.^(-x);
% Pedir al usuario funcion e intervalo de integracion.
% fun = input("Dame la funcion: ");
% a = input("Dame el limite inferior del intervalo de evaluacion: ");
% b = input("Dame el limite superior del intervalo de evaluacion: ");
a = 0;
b = 3;
% Grafica de la curva.
x1 = linspace(a,b);
y1 = fun(x1);
plot(x1,y1,'b')
hold on

% Numero de aplicaciones
n = input("Dame el numero de trapecios: ")

% Tamaño de pasos
h = (b-a)/n;

x = a:h:b;
y = fun(x);

I = zeros(n,1);

% Grafica de la pared izquierda de la primer aplicacion.
plot([x(1) x(1)],[0 y(1)],'g')
hold on

for i = 1:n
  base = x(i + 1) - x(i);
  alturaProm = (y(i) + y(i + 1))/2;
  I(i) = base*alturaProm;
  
  % Grafica de la pared derecha de la aplicacion
  plot([x(i + 1) x(i + 1)],[0 y(i + 1)],'g')
  hold on
end

Integral = sum(I)

% TRAMPA
% Calculo edl are "Real"
Real = quadl(fun,a,b)

% Calculo del error
ERPa = abs((Real - Integral) / Real) * 100