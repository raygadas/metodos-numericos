clc;
clear;
close all;

% A = [.5  1.1; .8 2.4; 1.5 5.3; 2.5 7.6; 4 8.9];
% A = [1 .5; 2 2.5; 3 2; 4 4; 5 3.5; 6 6; 7 5.5];
% A = input('Dame la matriz con los valores entre corchetes [x y; etc ] = ')
A = [5 17; 10 24; 15 31; 20 33; 25 37; 30 37; 35 40; 40 40; 45 42; 50 41] 

%if size(A)(1) < 2 || size(A)(2) != 2
 %   disp('Mala Matriz');
 %  return;
%endif

plot(A(:, 1), A(:, 2), 'o');

# Calcular los valores necesarios para la matriz de coeficientes
n = size(A)(1)
xi = sum(A(:, 1))
xi_sqr = sum(A(:, 1).^ 2)

# Crear la matriz de coeficientes
Coef = [ n  xi;
         xi xi_sqr ]

# Calcular los valores para el vector del lado derecho 
yi = sum(A(:, 2))
xiyi = sum(A(:, 1) .* A(:, 2))

# Crear el vector del lado derecho
derecho = [yi; xiyi]

# Encontrar el vector vertical de soluciones al sistema usando MatLab
sol = Coef\derecho

# Definir x graficar
x = linspace(min(A(:,1))-5, max(A(:,1))+5);

# Encontrar y del modelo y guardarlo en la tercera columna
A(:, 3) = sol(1) + sol(2) * (A(:, 1))

hold on

# Encontrar y = mx + b para hacer la gráfica de la recta
# la pendiente y el cruce con el eje y estan dados en la
# solucion al sistema de ecuaciones
m = sol(2);
b = sol(1);
y = m*x + b;
plot(x, y, 'r')

# Encontrar Sr = sum((y_medida - y_modelo)^2)
Sr = sum((A(:,2) - A(:,3)).^2)

# Encontrar St usando el promedio de y_medida
avg_y = mean(A(:,2));
St = sum((A(:,2)-avg_y).^2)

Sy = sqrt(St/(n-1))
Sy = std(A(:, 2))
Syx = sqrt(Sr/(n-2))
r2 = (St - Sr) / St
r = sqrt(r2)
pim = r2 * 100

