%   Interpolación Lineal
%   Fernando Lobato Meeser
%
close all;
clear;
clc;

A = [2 3; 3 1; 1 1; 5 2; -2 4; 0 -7];

plot(A(:, 1), A(:, 2), 'o');
hold on;

A = sortrows(A);

plot(A(:, 1), A(:, 2), 'b');

x = 2.5;

if (!(x >= min(A(:, 1)) && x <= max(A(:, 2))))
    disp('Nel men');
    return;
end

pos = sum(A(:, 1) < x)

x0 = A(pos, 1)
x1 = A(pos, 2)
y0 = A(pos + 1, 1)
y1 = A(pos + 1, 2)

y = y0 + ( (y1 - y0) / (x1 - x0)) * (x - x0);
plot(x, y, '*');