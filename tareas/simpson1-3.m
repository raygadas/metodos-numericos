clear;
clc;

fun = @(x) 1-exp(-(x));

a = 0;
b = 3;


x1 = linspace(a,b);
y1 = fun(x1);

plot(x1, y1, 'b');
hold on;

n = 1;

h = (b - a) / (2 * n);

x = a:h:b;
y = fun(x);

I = zeros(n, 1);

plot([x(1) x(1)], [0 y(1)], 'g');
hold on;

for i=1:n

    base = x(2*i + 1) - x(2*i - 1);

    altureProm = (y(2*i - 1) + 4*y(2*i) + y(2*i + 1)) / 6;
    I(i) = base * altureProm;

    plot([x(2*i + 1) x(2*i + 1)], [0 y(2*i + 1)], 'g')
    hold on;

    plot([x(2*i) x(2*i)], [0 y(2*i)], 'y')
    hold on;

    x1 = linspace(x(2*i - 1), x(2*i + 1));
    y1 = zeros(100, 1);
    A = [x(2*i + 1) y(2*i + 1); x(2*i) y(2*i);  x(2*i - 1) y(2*i - 1);];
    %LAGRANGE

    for k = 1:100
        L = ones(3, 1);
        for z = 1:3
            for j = 1:3
                if j != z
                    L(z) = ((x1(k) - A(j, 1))/ (A(z, 1) - A(j, 1))) .* L(z);
                end
            end
        end
        y1(k) = sum(L.*A(:, 2));
    end
    plot(x1, y1, 'm');
    hold on;

end


integral = sum(I)


Real = quadl(fun,a,b)

ERPa = abs((Real - integral)/ Real) * 100

