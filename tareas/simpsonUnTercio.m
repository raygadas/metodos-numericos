% Este programa calcula una estimacion del area bajo la curva implementando la regla de simpson 1/3.
% Codigo desarrollado por Diego Betanzos Esquer - A00231974
% 03 de noviembre de 2017.

close all
clear all
clc

% fun = @(x) 0.2 + 25*x - 200*x.^2 + 675*x.^3 -900*x.^4 + 400*x.^5;
fun = @(x) 1-e.^(-x);
a = 0;
b = 3;

% Grafica de la curva.
x1 = linspace(a,b);
y1 = fun(x1);
plot(x1,y1,'b')
hold on

% Numero de aplicaciones
n = input("Dame el numero de aplicaciones de Simpson 1/3 deseadas: ")

% Tamaño de pasos
h = (b-a)/(2*n);

x = a:h:b;
y = fun(x);

I = zeros(n,1);

% Grafica de la pared izquierda de la primer aplicacion.
plot([x(1) x(1)],[0 y(1)],'g')
hold on

for i = 1:n
  base = x(2*i + 1) - x(2*i - 1);
  alturaProm = (y(2*i - 1) + 4*y(2*i) + y(2*i + 1))/6;
  I(i) = base*alturaProm;
  
  % Grafica de la pared derecha de la aplicacion
  plot([x(2*i + 1) x(2*i + 1)],[0 y(2*i + 1)],'g')
  hold on
  
  % Grafica de la pared central de cada aplicacion
  plot([x(2*i) x(2*i)],[0 y(2*i)],'y')
  hold on
  
  % Grafica del techo de cada aplicacion (parabola)
  x1 = linspace(x(2*i-1),x(2*i+1));
  y1 = zeros(100,1);
  A = [x(2*i-1) y(2*i-1);x(2*i) y(2*i);x(2*i+1) y(2*i+1)];
  
  for k = 1:100
    L = ones(3,1);
    
    for z = 1:3
      for j = 1:3
        if (z != j)
          L(z) = ((x1(k) - A(j,1))/(A(z,1) - A(j,1))).*L(z);
        end
      end
    end
    y1(k) = sum(L.*A(:,2));
  end
  plot(x1,y1,'r')
  hold on
end

Integral = sum(I)

% TRAMPA
% Calculo edl are "Real"
Real = quadl(fun,a,b)

% Calculo del error
ERPa = abs((Real - Integral) / Real) * 100