% Autor: Andres Raygadas
% 13 / Noviembre / 2017
% Integración de Romberg

close all
clear all
clc

% Definimos limites inferior y superior
a = 0;
b = 1;

% Definimos la funcion
n = 5;
f = @(x) sin(x);

% Valor inicial de h
h = (b - a);

r(1,1) = (b - a) * (f(a) + f(b)) / 2;

for j = 2:n
	h = h/2;
    subtotal = 0;

    for i = 1:2^(j-2)
        subtotal = subtotal + f(a + (2 * i - 1) * h);
    end

    r(j,1) = r(j-1,1) / 2 + h * subtotal;

    for k = 2:j
        r(j,k) = (4^(k-1) * r(j,k-1) - r(j-1,k-1)) / (4^(k-1) - 1);
    end

end;

display("Romber: ")
display(r(n,n));